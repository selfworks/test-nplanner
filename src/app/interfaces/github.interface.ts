export interface RepositoryList {
  total_count: number;
  incomplete_results: boolean;
  items: Repository[];
}

export interface Repository {
  id: number;
  name: string;
  full_name: string;
  description: string;
  created_at: string;
  updated_at: string;
  language: string;
  url: string;
  svn_url: string;
}
