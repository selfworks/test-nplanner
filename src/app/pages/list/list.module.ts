import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list.component';
import { RouterModule, Routes } from '@angular/router';
import { SearchModule } from '../../components/search/search.module';
import { RepListModule } from '../../components/rep-list/rep-list.module';

const routes: Routes = [{ path: '', component: ListComponent }];

@NgModule({
  declarations: [ListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SearchModule,
    RepListModule
  ]
})
export class ListModule {
}
