import { Component, OnInit } from '@angular/core';
import { GithubService } from '../../services/github.service';
import { debounceTime, distinct, finalize } from 'rxjs/operators';
import { StateService } from '../../services/state.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {
  public wait: boolean;
  private changeStream$ = new Subject<string>();

  constructor(private github: GithubService,
              private state: StateService) {
  }

  ngOnInit(): void {
    this.changeStream$.asObservable()
      .pipe(
        debounceTime(300),
        distinct()
      )
      .subscribe(search => this.getData(search));
  }

  private getData(search: string): void {
    if (search) {
      this.wait = true;
      this.github.search(search)
        .pipe(finalize(() => this.wait = false))
        .subscribe(answer => this.state.list$.next(answer.items), () => this.clearList());
    } else {
      this.clearList();
    }
  }

  private clearList(): void {
    this.state.list$.next([]);
  }

  public changeSearch(search: string): void {
    this.changeStream$.next(search);
  }

}
