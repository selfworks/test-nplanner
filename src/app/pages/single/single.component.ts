import { Component, OnDestroy, OnInit } from '@angular/core';
import { Repository } from '../../interfaces/github.interface';
import { StateService } from '../../services/state.service';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-single',
  templateUrl: './single.component.html',
  styleUrls: ['./single.component.sass']
})
export class SingleComponent implements OnInit, OnDestroy {
  public repository: Repository;
  private subscription: Subscription;

  constructor(private state: StateService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const id = +params.id;
      if (id) {
        this.subscription = this.state.list$
          .pipe(map(list => list.filter(item => item.id === id)[0]))
          .subscribe(elem => this.repository = elem || {} as Repository);
      } else {
        this.repository = {} as Repository;
      }
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
