import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RepositoryList } from '../interfaces/github.interface';

@Injectable({
  providedIn: 'root'
})
export class GithubService {
  private url = 'https://api.github.com/';

  constructor(private http: HttpClient) {
  }

  public search(search: string): Observable<RepositoryList> {
    return this.http.get<RepositoryList>(this.url + `search/repositories?q=${search}`);
  }

}
