import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Repository } from '../interfaces/github.interface';

@Injectable({
  providedIn: 'root'
})
export class StateService {
  public list$ = new BehaviorSubject<Repository[]>([]);

  constructor() {
  }

}
