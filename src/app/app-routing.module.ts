import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: () => import('./pages/list/list.module').then(m => m.ListModule), pathMatch: 'full' },
  { path: 'item/:id', loadChildren: () => import('./pages/single/single.module').then(m => m.SingleModule) },
  { path: '**', redirectTo: '/', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
