import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RepListComponent } from './rep-list.component';
import { MatTableModule } from '@angular/material/table';
import { RouterModule } from '@angular/router';
import { MatSortModule } from '@angular/material/sort';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [RepListComponent],
  exports: [RepListComponent],
  imports: [
    CommonModule,
    RouterModule,
    MatTableModule,
    MatSortModule,
    MatButtonModule
  ]
})
export class RepListModule { }
