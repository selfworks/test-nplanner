import { Component, OnDestroy, OnInit } from '@angular/core';
import { StateService } from '../../services/state.service';
import { MatTableDataSource } from '@angular/material/table';
import { Repository } from '../../interfaces/github.interface';
import { BehaviorSubject, combineLatest, Subscription } from 'rxjs';

@Component({
  selector: 'app-rep-list',
  templateUrl: './rep-list.component.html',
  styleUrls: ['./rep-list.component.sass']
})
export class RepListComponent implements OnInit, OnDestroy {
  public dataSource: MatTableDataSource<Repository>;
  public displayedColumns: string[] = ['name', 'description', 'created', 'language'];
  public sort$ = new BehaviorSubject<string>('');
  private subscribtion: Subscription;

  constructor(public state: StateService) {
  }

  ngOnInit(): void {
    this.subscribtion = combineLatest(this.state.list$, this.sort$)
      .subscribe(([list, sort]) => {
        const newList = [...list];
        if (sort) {
          newList.sort((a, b) => a[sort] > b[sort] ? 1 : a[sort] < b[sort] ? -1 : 0);
        }
        this.dataSource = new MatTableDataSource(newList);
      });
  }

  public changeSort(sort: string): void {
    this.sort$.next(sort === this.sort$.value ? '' : sort);
  }

  ngOnDestroy(): void {
    this.subscribtion.unsubscribe();
  }

}
