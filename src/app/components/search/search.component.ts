import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.sass']
})
export class SearchComponent implements OnInit {
  @Input() public wait: boolean;
  @Output() private changeSearch = new EventEmitter<string>();
  public searchForm: FormGroup;

  constructor() {
  }

  ngOnInit(): void {
    this.setForm();
  }

  private setForm(): void {
    this.searchForm = new FormGroup({
      search: new FormControl('')
    });
    this.searchForm.controls.search.valueChanges.subscribe(value => this.changeSearch.emit(value));
  }

}
